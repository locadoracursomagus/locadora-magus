/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.model;


/**
 *
 * @author mcarolina
 */
public enum TipoMidia {
    DVD,
    BLURAY,
    CD,
    VHS
}
