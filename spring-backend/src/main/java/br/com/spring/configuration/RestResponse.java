/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.configuration;

/**
 *
 * @author mcarolina
 */
public class RestResponse<T extends Object> {
    
    private String message;
    private String type;
    private String details;
    private T value;

    public RestResponse() {
    }

    public RestResponse(String message, String type, String details, T value) {
        this.message = message;
        this.type = type;
        this.details = details;
        this.value = value;
    }

    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
    
    
}
