/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Funcionario;
import br.com.spring.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author mcarolina
 */
@Service
public class FuncionarioService {

    FuncionarioRepository repository;

    @Autowired
    private FuncionarioService(FuncionarioRepository repository) {
        this.repository = repository;
    }

    public RestResponse<Funcionario> salvar(Funcionario funcionario) {
        try {
            Funcionario func = repository.save(funcionario);
            return new RestResponse<>("Funcionário salvo com sucesso", "success", "", func);
        } catch (Exception e) {
            return new RestResponse<>("Erro ao salvar funcionário", "error", e.getMessage(), funcionario);
        }

    }

    public Funcionario buscarPorId(Long id) {
        return repository.findOne(id);
    }

    public void deletarPorId(Long id) {
        repository.delete(id);
    }

    public QueryObject<Funcionario> listarPaginado(QueryObject qo) {
        Page<Funcionario> pages = 
                qo.getSearch() == null || qo.getSearch().isEmpty()  ? 
                repository.findAll(new PageRequest(qo.getStart(), qo.getPageSize()))
                : repository.findByNomeLike(new PageRequest(qo.getStart(), qo.getPageSize()), "%"+qo.getSearch()+"%");
        qo.setCount(repository.count());
        qo.setValues(pages.getContent());
        return qo;
    }
}
