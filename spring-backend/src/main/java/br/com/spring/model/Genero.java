/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.model;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author mcarolina
 */
@Embeddable
public class Genero implements Serializable {

    private String genero;

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

}
