/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Cliente;
import br.com.spring.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author mcarolina
 */
@Service
public class ClienteService {

    ClienteRepository repository;

    @Autowired
    private ClienteService(ClienteRepository repository) {
        this.repository = repository;
    }

    public RestResponse<Cliente> salvar(Cliente cliente) {
        try {
            Cliente salvo = repository.save(cliente);
            return new RestResponse<>("Cliente salvo com sucesso", "success", "", salvo);
        } catch (Exception e) {
            return new RestResponse<>("Erros ao salvar ", "erros", e.getMessage(), cliente);
        }

    }

    public Cliente buscarPorId(Long id) {
        return repository.findOne(id);
    }

    public void deletarPorId(Long id) {
        repository.delete(id);
    }

    public QueryObject<Cliente> listarPaginado(QueryObject qo) {
        Page<Cliente> pages
                = qo.getSearch() == null || qo.getSearch().isEmpty()
                        ? repository.findAll(new PageRequest(qo.getStart(), qo.getPageSize()))
                        : repository.findByNomeLike(new PageRequest(qo.getStart(), qo.getPageSize()), "%" + qo.getSearch() + "%");
        qo.setCount(repository.count());
        qo.setValues(pages.getContent());
        return qo;
    }

}
