/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.repository;

import br.com.spring.configuration.AbstractRepository;
import br.com.spring.model.Filme;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author mcarolina
 */
public interface FilmeRepository extends AbstractRepository<Filme, Long>{
    
    public Page<Filme> findByNomeLike(Pageable pgbl,String nome);

}
