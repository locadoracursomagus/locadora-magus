/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.api;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Filme;
import br.com.spring.service.FilmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mcarolina
 */
//anotação que torna um controlador de url para o spring,,, anotação q diz quando a api vai ser disparada
@RestController
@RequestMapping("/filme")
public class FilmeAPI {

    @Autowired
    private FilmeService service;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public Filme getInstance() {
        return new Filme();
    }

    @RequestMapping(value = "/salvar", method = RequestMethod.POST)
    public RestResponse<Filme> salvar(@RequestBody Filme filme) {
        return service.salvar(filme);
    }

    @RequestMapping(value = "/editar")
    public String editar(@RequestBody Filme filme) {
        service.salvar(filme);
        return "Filme editado com sucesso";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Filme buscarPorId(@PathVariable("id") Long id) {
        return service.buscaPorId(id);
    }

    @RequestMapping(value = "/deletar/{id}", method = RequestMethod.GET)
    public void deletarPorId(@PathVariable("id") Long id) {
        service.deletarPorId(id);
//        return "Filme deletado com sucesso";
    }

    @RequestMapping(method = RequestMethod.GET)
    public QueryObject<Filme> listarPaginado(QueryObject q) {
        return service.listarPaginado(q);
    }

}
