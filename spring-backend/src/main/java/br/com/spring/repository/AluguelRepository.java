/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.repository;

import br.com.spring.configuration.AbstractRepository;
import br.com.spring.model.Aluguel;

/**
 *
 * @author SI 07
 */
public interface AluguelRepository extends AbstractRepository<Aluguel, Long>{
    
}
