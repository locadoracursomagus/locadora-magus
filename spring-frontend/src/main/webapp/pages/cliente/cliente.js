angular.module('cliente', [])
        .service('ClienteService', function ($http, ApiLocation) {
            this.salvar = function (dados) {
                return $http.post(ApiLocation.api + 'cliente/salvar', dados)
            }
            this.listar = function (page, size) {
                if (page == 1)
                    page = 0;
                if (page > 1)
                    page = page - 1;
                size = size || 10;
                return $http.get(ApiLocation.api + 'cliente?start=' + page + '&pageSize=' + size);
            }
            this.deletar = function (id) {
                return $http.get(ApiLocation.api + 'cliente/deletar/' + id)
            }

        })

        .controller('ClienteFormController', function ($scope, entity, ClienteService, $state) {
            $scope.entity = entity.data;
            $scope.salvar = function (entity) {
                ClienteService.salvar(entity).then(function (data) {
                    $state.go('clienteList');
                });
            }
        })
        .controller('ClienteListController', function ($scope, ClienteService) {
            $scope.page = 1;
            $scope.pageSize = 5;
            $scope.listar = function (page, size) {
                ClienteService.listar(page, size).then(function (data) {
                    console.log(data)
                    $scope.clientes = data.data;
                });
            };
            $scope.deletar = function (id) {
                ClienteService.deletar(id).then(function () {
                    $scope.listar($scope.page, $scope.pageSize);
                });

            };
            $scope.listar($scope.page, $scope.pageSize);
        });

