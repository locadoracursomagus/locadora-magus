/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.api;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Cliente;
import br.com.spring.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mcarolina
 */
@RestController
@RequestMapping("/cliente")
public class ClienteAPI {

    @Autowired
    ClienteService service;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public Cliente getInstance() {
        return new Cliente();
    }

    @RequestMapping(value = "/salvar", method = RequestMethod.POST)
    public RestResponse<Cliente> salvar(@RequestBody Cliente cliente) {
        return service.salvar(cliente);
        
    }

    public String editar(@RequestBody Cliente cliente) {
        service.salvar(cliente);
        return "Cliente alterado com sucesso";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Cliente buscarPorId(@PathVariable("id") Long id) {
        return service.buscarPorId(id);
    }

    @RequestMapping(value = "/deletar/{id}", method = RequestMethod.GET)
    public void deletarPorId(@PathVariable("id") Long id) {
        service.deletarPorId(id);
//        return "Deletado com sucesso";
    }

    @RequestMapping(method = RequestMethod.GET)
    public QueryObject<Cliente> listarPaginado(QueryObject q) {
        return service.listarPaginado(q);
    }

}
