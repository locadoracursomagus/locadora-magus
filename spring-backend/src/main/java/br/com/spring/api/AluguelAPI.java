/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.api;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Aluguel;
import br.com.spring.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SI 07
 */
@RestController
@RequestMapping("/aluguel")
public class AluguelAPI {

    @Autowired
    private AluguelService service;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public Aluguel getInstance() {
        return new Aluguel();
    }

    @RequestMapping(value = "/salvar", method = RequestMethod.POST)
    public RestResponse<Aluguel> salvar(@RequestBody Aluguel aluguel) {
        return service.salvar(aluguel);
    }

    @RequestMapping(value = "/editar")
    public RestResponse<Aluguel> editar(@RequestBody Aluguel filme) {
        return service.salvar(filme);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Aluguel buscarPorId(@PathVariable("id") Long id) {
        return service.buscaPorId(id);
    }

    @RequestMapping(value = "/deletar/{id}", method = RequestMethod.GET)
    public void deletarPorId(@PathVariable("id") Long id) {
        service.deletarPorId(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public QueryObject<Aluguel> listarPaginado(QueryObject q) {
        return service.listarPaginado(q);
    }

}
