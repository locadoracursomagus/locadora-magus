/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Aluguel;
import br.com.spring.repository.AluguelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author SI 07
 */
@Service
public class AluguelService {
    
     AluguelRepository repository;

    // ficar presso a uma instancaia só (singliton)
    @Autowired
    private AluguelService(AluguelRepository repository) {
        this.repository = repository;
    }
    
    public RestResponse<Aluguel> salvar(Aluguel aluguel) {
         try {
            Aluguel salvo = repository.save(aluguel);
            return new RestResponse<>("Aluguel salvo com sucesso", "success", "", salvo);
        } catch (Exception e) {
            return new RestResponse<>("Erros ao salvar ", "erros", e.getMessage(), aluguel);
        }
    }
    
    public Aluguel buscaPorId(Long id) {
        return repository.findOne(id);
    }
    
    public void deletarPorId(Long id) {
        repository.delete(id);
    }
    
    public QueryObject<Aluguel> listarPaginado(QueryObject qo) {
        Page<Aluguel> pages = repository.findAll(new PageRequest(qo.getStart(), qo.getPageSize()));
        qo.setCount(repository.count());
        qo.setValues(pages.getContent());
        return qo;
    }
}
    

