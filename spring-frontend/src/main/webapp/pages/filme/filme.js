angular.module('filme', [])
        .service('FilmeService', function ($http, ApiLocation) {
            this.salvar = function (dados) {
                return $http.post(ApiLocation.api + 'filme/salvar', dados)
            }
            this.listar = function (page, size) {
                if (page == 1)
                    page = 0;
                if (page > 1)
                    page = page - 1;
                size = size || 10;
                return $http.get(ApiLocation.api + 'filme?start=' + page + '&pageSize=' + size);

            }
            this.deletar = function (id) {
                return $http.get(ApiLocation.api + 'filme/deletar/' + id)
            }
        })


        .controller('FilmeFormController', function ($scope, entity, FilmeService, $state) {
            $scope.entity = entity.data;
            $scope.salvar = function (entity) {
                FilmeService.salvar(entity).then(function (data) {
                    console.log(data)
                    $state.go('filmeList');
                })
            }

        })
        .controller('FilmeListController', function ($scope, FilmeService) {
            $scope.page = 1;
            $scope.pageSize = 5;
            $scope.listar = function (page, size) {
                FilmeService.listar(page, size).then(function (data) {
                    console.log(data)
                    $scope.filmes = data.data;
                });
            };
            $scope.deletar = function (id) {
                FilmeService.deletar(id).then(function () {
                    $scope.listar($scope.page, $scope.pageSize);
                });

            };

            $scope.listar($scope.page, $scope.pageSize);

        });

