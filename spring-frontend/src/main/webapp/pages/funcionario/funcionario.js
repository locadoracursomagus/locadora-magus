angular.module('funcionario', [])
        .service('FuncionarioService', function ($http, ApiLocation) {
            this.salvar = function (dados) {
                return $http.post(ApiLocation.api + 'funcionario/salvar', dados)
            }
            this.listar = function (page, size) {
                if (page == 1)
                    page = 0;
                if (page > 1)
                    page = page - 1;
                size = size || 10;
                return $http.get(ApiLocation.api + 'funcionario?start=' + page + '&pageSize=' + size);
            }
            this.deletar = function (id) {
                return $http.get(ApiLocation.api + 'funcionario/deletar/' + id)
            }
        })


        .controller('FuncionarioListController', function ($scope, FuncionarioService) {
            $scope.page = 1;
            $scope.pageSize = 5;
            $scope.listar = function (page, size) {
                FuncionarioService.listar(page, size).then(function (data) {
                    $scope.funcionarios = data.data;
                });
            };
            $scope.deletar = function (id) {
                FuncionarioService.deletar(id).then(function (data) {
                    $scope.listar($scope.page, $scope.pageSize);
                });
            };
            $scope.listar($scope.page, $scope.pageSize);
        })
        .controller('FuncionarioFormController', function ($scope, entity, FuncionarioService, $state) {
            $scope.entity = entity.data;
            $scope.salvar = function (entity) {
                FuncionarioService.salvar(entity).then(function (data) {
                    $state.go('funcionarioList');
                });
            };
        });