/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mcarolina
 */
@Entity
public class Cliente extends Pessoa {

    @Temporal(TemporalType.DATE)
    private Date dataAberturaCadastro;
    private String qualificacao;

    public Date getDataAberturaCadastro() {
        return dataAberturaCadastro;
    }

    public void setDataAberturaCadastro(Date dataAberturaCadastro) {
        this.dataAberturaCadastro = dataAberturaCadastro;
    }

    public String getQualificacao() {
        return qualificacao;
    }

    public void setQualificacao(String qualificacao) {
        this.qualificacao = qualificacao;
    }

}
