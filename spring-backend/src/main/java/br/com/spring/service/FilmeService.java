/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Filme;
import br.com.spring.repository.FilmeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author mcarolina
 */
@Service
public class FilmeService {

    FilmeRepository repository;

    // ficar presso a uma instancaia só (singleton)
    @Autowired
    private FilmeService(FilmeRepository repository) {
        this.repository = repository;
    }

    public RestResponse<Filme> salvar(Filme filme) {
        try {
            Filme salvo = repository.save(filme);
            return new RestResponse<>("Filme salvo com sucesso", "success", "", salvo);
        } catch (Exception e) {
            return new RestResponse<>("Erro ao salvar filme", "error", e.getMessage(), filme);
        }

    }

    public Filme buscaPorId(Long id) {
        return repository.findOne(id);
    }

    public void deletarPorId(Long id) {
        repository.delete(id);
    }

    public QueryObject<Filme> listarPaginado(QueryObject qo) {
        Page<Filme> pages = 
                qo.getSearch() == null || qo.getSearch().isEmpty()  ? 
                repository.findAll(new PageRequest(qo.getStart(), qo.getPageSize()))
                : repository.findByNomeLike(new PageRequest(qo.getStart(), qo.getPageSize()), "%"+qo.getSearch()+"%");
        qo.setCount(repository.count());
        qo.setValues(pages.getContent());
        return qo;
    }
    
    
}
