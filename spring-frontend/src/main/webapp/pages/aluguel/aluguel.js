angular.module('aluguel', [])
        .service('AluguelService', function ($http, ApiLocation) {
            this.salvar = function (dados) {
                return $http.post(ApiLocation.api + 'aluguel/salvar', dados)
            }
            this.listar = function (page, size) {
                if (page == 1)
                    page = 0;
                if (page > 1)
                    page = page - 1;
                size = size || 10;
                return $http.get(ApiLocation.api + 'aluguel?start=' + page + '&pageSize=' + size);
            }
            this.deletar = function (id) {
                return $http.get(ApiLocation.api + 'aluguel/deletar/' + id)
            }

        })
        .controller('AluguelFormController', function ($scope, entity, $http, ApiLocation, AluguelService, $state) {
            $scope.entity = entity.data;
            console.log(entity.data)
            $scope.loadFilmes = function (query) {
                return $http.get(ApiLocation.api + '/filme?search=' + query)
                        .then(function (data) {
                            data.data = data.data.values;
                            return data;
                        })
            };

            $scope.getClientes = function (val) {
                return $http.get(ApiLocation.api + '/cliente?search=' + val)
                    .then(function (response) {
                    return response.data.values;
                });
            };
            
            $scope.getFuncionarios = function (val) {
                return $http.get(ApiLocation.api + '/funcionario?search=' + val)
                    .then(function (response) {
                    return response.data.values;
                });
            };

            $scope.salvar = function (entity) {
                AluguelService.salvar(entity).then(function (data) {
                    $state.go('aluguelList');
                });
            }

        })

        .controller('AluguelListController', function ($scope,AluguelService) {
            $scope.page = 1;
            $scope.pageSize = 5;
            
            $scope.listar = function (page, size) {
                AluguelService.listar(page, size).then(function (data) {
                    $scope.aluguels = data.data;
                });
            };
            
            $scope.deletar = function (id) {
                AluguelService.deletar(id).then(function () {
                    $scope.listar($scope.page, $scope.pageSize);
                });

            };
            $scope.listar($scope.page, $scope.pageSize);

        })



