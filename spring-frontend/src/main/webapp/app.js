angular.module('locadora', ['ngTagsInput','ui.router', 'filme', 'aluguel', 'cliente', 'funcionario', 'apiLocation', 'ui.bootstrap'])
        .config(function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('welcome');

            $stateProvider
                    .state('welcome', {
                        url: '/welcome',
                        templateUrl: 'pages/welcome/welcome.html'

                    })
                    .state('filmeForm', {
                        url: '/filme/insert',
                        templateUrl: 'pages/filme/view/form.html',
                        controller: 'FilmeFormController',
                        resolve: {
                            entity: function ($http, ApiLocation) {
                                return $http.get(ApiLocation.api + 'filme/new')
                                        .success(function (data) {
                                            return data;
                                        })
                            }
                        }

                    })
                    .state('filmeEdit', {
                        url: '/filme/edit/:id',
                        templateUrl: 'pages/filme/view/form.html',
                        controller: 'FilmeFormController',
                        resolve: {
                            entity: function ($http, ApiLocation, $stateParams) {
                                return $http.get(ApiLocation.api + 'filme/' + $stateParams.id)
                                        .success(function (data) {
                                            return data;
                                        });
                            }
                        }
                    })
                    .state('filmeList', {
                        url: '/filme',
                        templateUrl: 'pages/filme/view/list.html',
                        controller: 'FilmeListController'
                    })

                    // Cliente ----------------------------------------------------------------------------
                    .state('clienteForm', {
                        url: '/cliente/insert',
                        templateUrl: 'pages/cliente/view/form.html',
                        controller: 'ClienteFormController',
                        resolve: {
                            entity: function ($http, ApiLocation) {
                                return $http.get(ApiLocation.api + 'cliente/new')
                                        .success(function (data) {
                                            return data;
                                        })
                            }
                        }

                    })
                    .state('clienteEdit', {
                        url: '/cliente/edit/:id',
                        templateUrl: 'pages/cliente/view/form.html',
                        controller: 'ClienteFormController',
                        resolve: {
                            entity: function ($http, ApiLocation, $stateParams) {
                                return $http.get(ApiLocation.api + 'cliente/' + $stateParams.id)
                                        .success(function (data) {
                                            return data;
                                        });
                            }
                        }
                    })
                    .state('clienteList', {
                        url: '/cliente',
                        templateUrl: 'pages/cliente/view/list.html',
                        controller: 'ClienteListController'
                    })

                    // Aluguel -------------------------------------------------------------------------
                    .state('aluguelForm', {
                        url: '/aluguel/insert',
                        templateUrl: 'pages/aluguel/view/form.html',
                        controller: 'AluguelFormController',
                        resolve: {
                            entity: function ($http, ApiLocation) {
                                return $http.get(ApiLocation.api + 'aluguel/new')
                                        .success(function (data) {
                                            return data;
                                        })
                            }
                        }

                    })
                    .state('aluguelEdit', {
                        url: '/aluguel/edit/:id',
                        templateUrl: 'pages/aluguel/view/form.html',
                        controller: 'AluguelFormController',
                        resolve: {
                            entity: function ($http, ApiLocation, $stateParams) {
                                return $http.get(ApiLocation.api + 'aluguel/' + $stateParams.id)
                                        .success(function (data) {
                                            return data;
                                        });
                            }
                        }
                    })
                    .state('aluguelList', {
                        url: '/alugel',
                        templateUrl: 'pages/aluguel/view/list.html',
                        controller: 'AluguelListController'
                    })

                    // Funcionario -----------------------------------------------------------------------
                    .state('funcionarioList', {
                        url: '/funcionario',
                        templateUrl: 'pages/funcionario/view/list.html',
                        controller: 'FuncionarioListController'
                    })
                    .state('funcionarioForm', {
                        url: '/funcionario/insert',
                        templateUrl: 'pages/funcionario/view/form.html',
                        controller: 'FuncionarioFormController',
                        resolve: {
                            entity: function ($http, ApiLocation) {
                                return $http.get(ApiLocation.api + 'funcionario/new')
                                        .success(function (data) {
                                            return data;
                                        })
                            }
                        }
                    })
                    .state('funcionarioEdit', {
                        url: '/funcionario/edit/:id',
                        templateUrl: 'pages/funcionario/view/form.html',
                        controller: 'FuncionarioFormController',
                        resolve: {
                            entity: function ($http, ApiLocation, $stateParams) {
                                return $http.get(ApiLocation.api + 'funcionario/' + $stateParams.id)
                                        .success(function (data) {
                                            return data;
                                        });
                            }
                        }
                    });
        })


