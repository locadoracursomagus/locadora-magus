/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.api;

import br.com.spring.configuration.QueryObject;
import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Funcionario;
import br.com.spring.service.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mcarolina
 */
@RestController
@RequestMapping("/funcionario")
public class FuncionarioAPI {

    @Autowired
    private FuncionarioService service;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public Funcionario getIntance() {
        return new Funcionario();
    }

    @RequestMapping(value = "/salvar", method = RequestMethod.POST)
    public RestResponse<Funcionario> salvar(@RequestBody Funcionario funcionario) {
        return service.salvar(funcionario);
    }

    @RequestMapping(value = "/editar")
    public String editar(@RequestBody Funcionario funcionario) {
        service.salvar(funcionario);
        return "Alterado com sucesso";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Funcionario buscarPorId(@PathVariable("id") Long id) {
        return service.buscarPorId(id);
    }

    @RequestMapping(value = "/deletar/{id}", method = RequestMethod.GET)
    public void deletarPorId(@PathVariable("id") Long id) {
        service.deletarPorId(id);
//        return "Deletado com sucesso";
    }

    @RequestMapping(method = RequestMethod.GET)
    public QueryObject<Funcionario> listarPaginado(QueryObject q) {
        return service.listarPaginado(q);
    }
}
